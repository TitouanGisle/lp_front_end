$(document).ready(function() {
    setUpModal();
})

/**
 * Ajoute des listeners aux boutons des sections
 * pour afficher et modifier la modale
 */
function setUpModal() {

    //Une classe aurait été préférable, modifierais si j'avais le temps
    $('#add-info').click(function(event) {
        $('#modal-label').html('Ajouter une information');
        $("#form-ajout-info").submit(populateInfo());
        $('#ajout-infos-modal').modal('show');
    });

    $('#add-comp').click(function(event) {
        $('#modal-label').html('Ajouter une compétence');
        $("#form-ajout-info").submit(populateCompetences())
        $('#ajout-infos-modal').modal('show');
    });

    $('#add-exp').click(function(event) {
        $('#modal-label').html('Ajouter une expérience');

        /*ajout d'autres input au formulaire sur la modale
        ...
        */

        $("#form-ajout-info").submit(populateExperiences())
        $('#ajout-infos-modal').modal('show');

    });
}

/**
 * Ajoute le texte passé en paramètre comme item de la liste d'informations
 * @param {*} event 
 */
function populateInfo() {
    //event.preventDefault();
    var text = $('[name=info-input').val();
    $('#liste-infos').append('<li>'+text+'</li>');
    $('#ajout-infos-modal').modal('hide');
}

