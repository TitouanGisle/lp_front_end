class Creature {
    constructor(nom, image, description, date_crea) {
        this.nom = nom;
        this.image = "./resources/img/"+image;
        this.description = description;
        this.date_crea = date_crea;
    }

    /**
     * Retourne la créature, mise en forme pour un conteneur bootsrap
     */
    asTableRow() {
        return '<div class="creat-container col-lg-4 col-md-6 col-sm-12 text-center mb-4"><p>'+
   
        this.nom+
        '</p> <p><img src=\"'+
            this.image+
        '\"</p><p>'+
            this.description+
        '</p><p>'+
            this.date_crea+
        '</p><p><button type="button" name="button" value="Découvrir" class="consultation-creature btn btn-info mr-4" data-nom="'+this.nom+'">Découvrir</button>'+
        '<button type="button" class="edition-creature btn btn-secondary mr-4" data-nom="'+this.nom+'">Editer</button>'+
        '<button type="button" class="suppression-creature btn btn-danger" data-nom="'+this.nom+'">Supprimer</button></p></div>';
    }
}

var liste_creatures = [
    new Creature("Equus phocidae", "cheval-phoque.png", "Le croisement audacieux entre un pur-sang arabe, et un phoque !", "Juin 2016"),
    new Creature("Strix canis", "Bulldog-owl.jpg", "Le superbe mélange entre une chouette hulotte et un bulldog français", "Septembre 2017"),
    new Creature("Homo platyrhynchos", "human-duck.jpg", "L'accouplement caucasse entre un canard colvert et un acteur de cinéma", "Janvier 2012"),
    new Creature("Lasius Equus zebra", "zebra-ant.jpg", "Le métissage particulièrement technique entre une fourmi noire et un zèbre d'Afrique", "Décembre 2019"),
    new Creature("Smilrk", 'smilrk.webp', "On a un peu foiré sur ce coup là...", "13 Décembre 2019")
]

/**
 * Affichage des créatures,
 * et préparation des événements des différents boutons
 */
$(document).ready(function() {
    populateTableauCreatures();
});


/**
 * Remplit le tableau des créatures, avec la liste des créatures puis set les events
 */
function populateTableauCreatures() { 
    var table_creatures = $('#table-creatures');
    table_creatures.empty();
    liste_creatures.forEach(creature => {
        table_creatures.append(creature.asTableRow());
    });

    setEvents();
}


/**
 * Ajoute des événements à tous les différents boutons
 */
function setEvents() {
    $('.consultation-creature').click(function(event){
        afficherCreature(event);
    });

    $('#ajout-creature').click(function(event){
        prepareAjoutCreature(event); 
    });

    $('.edition-creature').click(function(event) {
        prepareEditionCreature(event);
    });

    $('.suppression-creature').click(function(event) {
        suppressionCreature(event);
    });
}

function afficherCreature(event) {
    var creature_nom = $(event.target).data('nom');
    console.log(creature_nom);
    var creature = getCreatureByNom(creature_nom);

    $('#consultation-creature-modal-label').html(creature.nom);
    $('#creat-img').attr('src', creature.image);
    $('#creat-description').html(creature.description);
    $('#creat-date').html(creature.date_crea);

    $('#consultation-creature-modal').modal('show');
}


/**
 * Supprime une créature du tableau
 */
function suppressionCreature(event) {
    var nom_suppr = $(event.target).data('nom');
    if (confirm("Êtes-vous sûr de vouloir supprimer "+nom_suppr+" ?")) {
        for( var i = 0; i < liste_creatures.length; i++){ 
            if ( liste_creatures[i].nom == nom_suppr) {
                liste_creatures.splice(i, 1);
            }
        }
        populateTableauCreatures();
    }
}

/**
 * Prépare la modale à l'ajout d'une créature :
 * Vide les input de la modale, et modifie le comportement du formulaire
 */
function prepareAjoutCreature(event) {
    //vidage de la modale
    $('[name=nom_creature]').val('');
    $('[name=image_creature]').val('');
    $('[name=description_creature]').val('');
    $('[name=date_crea]').val('');
    $('#ajout-creature-modal-label').html('Ajouter une créature');


    //suppression des listeners précédents
    $('form').unbind();

    //ajout listener au formulaire
    $("form").submit(function(e){
        e.preventDefault();
        var new_crea = new Creature($('[name=nom_creature]').val(),
                    $('[name=image_creature]').val(),
                    $('[name=description_creature]').val(),
                    $('[name=date_crea]').val());
        liste_creatures.push(new_crea);
        populateTableauCreatures();
        $('#ajout-creature-modal').modal('hide')
    });
}


/**
 * Prépare la modale à l'édition d'une créature :
 * Pré-remplit les input, et modifie le comportement du formulaire
*/
function prepareEditionCreature(event) {

    //récupération créature
    var creature = getCreatureByNom($(event.target).attr('data-nom'));

    //remplissage de la modale
    $('[name=nom_creature]').val(creature.nom);
    $('[name=image_creature]').val(creature.image);
    $('[name=description_creature]').val(creature.description);
    $('[name=date_crea]').val(creature.date_crea);
    $('form').attr('data-nom', creature.nom);
    $('#ajout-creature-modal-label').html('Editer la créature');

    //suppression des listeners précédents sur le formulaire
    $('form').unbind();

    //ajout listener au formulaire
    $("form").submit(function(e){
        e.preventDefault();
        var creature = getCreatureByNom($(this).attr('data-nom'));
        creature.nom = $('[name=nom_creature]').val();
        creature.image = $('[name=image_creature]').val();
        creature.description = $('[name=description_creature]').val();
        creature.date_crea = $('[name=date_crea]').val();
        //liste_creatures.push(creature);
        populateTableauCreatures();
        $('#ajout-creature-modal').modal('hide');
    });

    $('#ajout-creature-modal').modal('show');
}

/**
 * Renvoie la créature de l'array dont le nom est passé en paramètre.
 * @param {string} nom_creature 
 * @return Creature/null
 */
function getCreatureByNom(nom_creature) {
    for( var i = 0; i < liste_creatures.length; i++){ 
        if ( liste_creatures[i].nom == nom_creature) {
            return liste_creatures[i];
        }
    }
    return null;
}