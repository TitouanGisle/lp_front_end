var villes = [["Paris", "Annecy", "Lyon", "Grenoble"],
            ["Rome", "Florence", "Venise", "Turin"],
            ["Madrid", "Barcelone", "Tolède", "Grenade"],
            ["Londres", "York", "Manchester"]];
var pays = ["France", "Italie", "Espagne", "Angleterre"];
 
$(document).ready(function() {
    $("#button-submit").click(function() {
        var input_ville = $('#ville-input').val();
        let input_pays = getPaysFromVille(input_ville);
        if(input_pays==null) {
            alert("On ne connait pas cette ville...");
        } else {
            readySuggestionsFor(input_pays);
            $("#suggestions").show();
        }
    });

})

/**
 * Retourne le pays contenant le ville passée en paramètre, ou null
 * @param {string} in_ville 
 */
function getPaysFromVille(in_ville) {
    var index = 0;
    while(index<pays.length && !villes[index].includes(in_ville)) {
        index++;
    }
   return index<pays.length ? pays[index] : null;
}

/**
 * Affiche la liste des villes correspondant au pays passé en paramètre
 * @param {string} in_pays 
 */
function readySuggestionsFor(in_pays) {
    var villes_suggerees = villes[pays.indexOf(in_pays)];

    var ul = document.getElementById('liste_suggestions');

    villes_suggerees.forEach(ville => {
        let li = document.createElement('li');
        li.innerHTML=li.innerHTML + ville;
        ul.appendChild(li);
    });

    readyNewCityInput();
}

/**
 * Change le comportement du bouton après sélection d'une ville
 */
function readyNewCityInput() {
    //suppression de l'événement original
    $("#button-submit").unbind("click");
    //ajout du nouvel événement
    $("#button-submit").click(function(){
        var ul = $('#liste_suggestions');
        var input_ville = $("#ville-input").val();
        let li = "<li>"+input_ville+"</li>";
        if( confirm("Ajouter "+input_ville+" à la liste ?")) {
            ul.append(li);
        }
    });
}