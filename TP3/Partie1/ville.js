var villes = [["Paris", "Annecy", "Lyon", "Grenoble"],
            ["Rome", "Florence", "Venise", "Turin"],
            ["Madrid", "Barcelone", "Tolède", "Grenade"],
            ["Londres", "York", "Manchester"]];
var pays = ["France", "Italie", "Espagne", "Angleterre"];
 
window.onload = function(){
    document.getElementById("button-submit").addEventListener("click", checkInput);
};

function checkInput() {
    var input_ville = document.getElementById("ville-input").value;
    let input_pays = getPaysFromVille(input_ville);
    if(input_pays==null) {
        alert("On ne connait pas cette ville...");
    } else {
        readySuggestionsFor(input_pays);
        document.getElementById("suggestions").setAttribute('style', 'display: block;');
    }
}

/**
 * Retourne le pays contenant le ville passée en paramètre, ou null
 * @param {string} in_ville 
 */
function getPaysFromVille(in_ville) {
    var index = 0;
    while(index<pays.length && !villes[index].includes(in_ville)) {
        index++;
    }
   return index<pays.length ? pays[index] : null;
}

function readySuggestionsFor(in_pays) {
    var villes_suggerees = villes[pays.indexOf(in_pays)];

    var ul = document.getElementById('liste_suggestions');

    villes_suggerees.forEach(ville => {
        let li = document.createElement('li');
        li.innerHTML=li.innerHTML + ville;
        ul.appendChild(li);
    });

    readyNewCityInput();
}

function readyNewCityInput() {
    //suppression de l'événement original
    document.getElementById("button-submit").removeEventListener("click", checkInput);
    //ajout du nouvel événement
    document.getElementById("button-submit").addEventListener("click", addVille);
}

function addVille(event) {
    var ul = document.getElementById('liste_suggestions');
    var input_ville = document.getElementById("ville-input").value;
    let li = document.createElement('li');
    li.innerHTML=input_ville;
    if( confirm("Ajouter "+input_ville+" à la liste ?")) {
        ul.appendChild(li);
    }
}