class Creature {
    constructor(nom, image, description, date_crea) {
        this.nom = nom;
        this.image = "./resources/img/"+image;
        this.description = description;
        this.date_crea = date_crea;
    }

    /**
     * Retourne la créature, mise en forme pour un tableau
     */
    asTableRow() {
        return "<tr><td>"+
            this.nom+
        "</td> <td><img src=\""+
            this.image+
        "\"</td><td>"+
            this.description+
        "</td><td>"+
            this.date_crea+
        '</td><td><button type="button" name="button" value="Découvrir">Découvrir</button>'+
        '<button type="button" class="supprimer_crea" data-nom="'+this.nom+'">Supprimer</bouton></td></tr>';
    }
}

var liste_creatures = [
    new Creature("Equus phocidae", "cheval-phoque.png", "Le croisement audacieux entre un pur-sang arabe, et un phoque !", "Juin 2016"),
    new Creature("Strix canis", "Bulldog-owl.jpg", "Le superbe mélange entre une chouette hulotte et un bulldog français", "Septembre 2017"),
    new Creature("Homo platyrhynchos", "human-duck.jpg", "L'accouplement caucasse entre un canard colvert et un acteur de cinéma", "Janvier 2012"),
    new Creature("Lasius Equus zebra", "zebra-ant.jpg", "Le métissage particulièrement technique entre une fourmi noire et un zèbre d'Afrique", "Décembre 2019")
]

$(document).ready(function() {
    populateTableauCreatures();
    ajoutCreature();
    editionCreature();
    suppressionCreature();
});

/**
 * Remplit le tableau des créatures, avec la liste des créatures 
 */
function populateTableauCreatures() { 
        var table_creatures = $('#table-creatures');
        table_creatures.empty();
        liste_creatures.forEach(creature => {
            table_creatures.append(creature.asTableRow());
        });
}

function ajoutCreature() {
    $("form").submit(function(e){
        e.preventDefault();
        var new_crea = new Creature($('[name=nom_creature]').val(),
                    $('[name=image_creature]').val(),
                    $('[name=description_creature]').val(),
                    $('[name=date_crea]').val());
        liste_creatures.push(new_crea);
        populateTableauCreatures();
    })
}

function suppressionCreature() {
    $('.supprimer_crea').click(function(e) {
        console.log('suppression !');
        var nom_suppr = $(this).attr('data-nom');
        for( var i = 0; i < liste_creatures.length; i++){ 
            if ( liste_creatures[i].nom == nom_suppr) {
                liste_creatures.splice(i, 1);
            }
        }
        populateTableauCreatures();
    });
}
/**
 * Faite durant le tp4
 */
function editionCreature() {

}
