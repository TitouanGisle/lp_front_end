class MenuItem {
    constructor(nom, href, dropdown = null) {
        this.nom = nom;
        this.href = href;
        this.dropdown = dropdown;
    }

    asListItem() {
        let li = this.dropdown!=null ? '<li class="dropdown">':'<li>';
        li += '<a href="'+this.href+'">'+this.nom+'</a>';
        li += this.dropdown!=null ? this.generateDropDown()+'</li>' : '</li>';
        return li;
    }

    generateDropDown() {
        var dropdown = '<div class=dropdown-content>';
        this.dropdown.forEach(item => {
            dropdown+='<a href="#">'+item+'</a>';
        });
        dropdown+='</div>';
        return dropdown;
    }
}

var menu_items = [new MenuItem('Menu', "./menu.html", ['Connexion', 'Billeterie', 'Recherche', 'Promotions']),
            new MenuItem('Activités', './activites.html', ['Actualités', 'Programme', 'Tarifs', 'Parrainage et conservation']),
            new MenuItem('Nos Créatures', './creatures.html'),
            new MenuItem('Banque de son', './audio.html'),
            new MenuItem('Visite Virtuelle Vidéo', './video.html'),
            new MenuItem('Contact', "./contact.html")];

$(document).ready(function() {
    generateMenu();
});

function generateMenu() {
    let menu_element = $('#menu');
    menu_items.forEach(item => {
        menu_element.append(item.asListItem());
    });
}